# xlxreflector

Transcoding XLX reflector setup with transcoding capabilities!

Beware, by using code from this repository, you are already three feet deep in uncharted territory. In order to become more familiar with ham radio for digital voice modes, I decided to set up a XLXReflector with transcoding capabilities with a DVMega DVStick 33.
My XLX reflector setup is managed using Ansible playbooks in this repository. With this configuration, I aim to maintain the manageability of my reflector and guarantee that the setup can be repeated on every occasion. Ansible Semaphore is the operator of the play books in this repository.

If you have any questions, please feel free to reach out by email. I am happy to accept pull requests if you plan to use the code and want to contribute.

This repository might eventually turn into a full Ansible role.


# Use the ansible playbooks

As of now, there is no role that include all the tasks and that is ready to use. However; you can use ansible directly to run the playbooks and get to a working server Obviously you need to install ansible first. Please check the ansible documentation on how to install Ansible. On the server (the xlxreflector), you need to have a user that you can *ssh* into and that can run *sudo* commands without passwords.

On most systems the package manager that comes with the systems has a recent version packed.

    # On Debian'ish system
    apt install ansible

    # On RHEL based systems
    yum install ansible-core

    # On Fedora
    dnf install ansible-core

After the installation run the following commands to check out the sources for this Ansible playbooks.

    # On your local system
    git clone https://codeberg.org/rhaen/xlxreflector.git
    cd xlxreflector

# Running the playbooks

You can run the ansible playbooks straight from the commandline. Please replace the required information with valid information such as your hostname, etc.

    # Update your server with the latest updates (notice the trailing comma!)
    ansible-playbook playbooks/sysadmin/update-apt-packages.yaml -i <hostname of your server>,

    # Install iptables filtering and allow required connections (notice the trailing comma!)
    ansible-playbook playbooks/xlxreflector/security.yaml -i <hostname of your server>,

    # Install the XLXD reflector software
    ansible-playbook playbooks/xlxreflector/install.yaml -i xlx502.de, \
      -e xrfnum=<xlx reflector number>     \
      -e xlxdomain=<fqdn of server>        \
      -e email=<admin email address>       \
      -e callsign=<your callsign> 

Uli/DO7HBL
